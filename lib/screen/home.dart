import 'package:flutter/material.dart';
import 'package:flutter_application_login/screen/login.dart';
import 'package:flutter_application_login/screen/register.dart';

class homeScreen extends StatelessWidget {
  const homeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register & Login"),
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
        child: SingleChildScrollView(
          child: Column(children: [
            Image.asset("assets/images/m00.jpg"),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return registerScreen();
                    }),
                  );
                },
                icon: Icon(Icons.add),
                label: Text(
                  "สร้างบัญชี",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton.icon(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) {
                      return loginScrenn();
                    }),
                  );
                },
                icon: Icon(Icons.login),
                label: Text(
                  "เข้าสู่ระบบ",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
